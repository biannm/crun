Name:            crun
Version:         1.8.1
Release:         1
Summary:         A fast and low-memory footprint OCI Container Runtime fully written in C.
URL:             https://github.com/containers/%{name}
Source0:         https://github.com/containers/crun/releases/download/%{version}/%{name}-%{version}.tar.xz
License:         GPLv2+ and LGPLv2.1+
BuildRequires:   autoconf automake gcc python
BuildRequires:   libcap-devel systemd-devel yajl-devel libseccomp-devel libselinux-devel
BuildRequires:   libtool make glibc-static protobuf-c-devel
%ifnarch %ix86
BuildRequires: criu-devel >= 3.15
%endif
Provides:        oci-runtime

%description
crun is a fast and low-memory footprint OCI Container Runtime fully written in C.

%package help
Summary:         Secondary development document and manual of interface function description.
%description help
Secondary development document and manual of interface function description.

%prep
%autosetup -p1 -n %{name}-%{version}

%build
./autogen.sh
%configure --disable-silent-rules
%make_build

%install
%make_install
rm -rf %{buildroot}%{_prefix}/lib*

%files
%license COPYING
%license COPYING.libcrun
%{_bindir}/%{name}

%files help
%{_mandir}/man1/*

%changelog
* Wed Mar 15 2023 biannm <bian_naimeng@hoperun.com> - 1.8.1-1
- update to 1.8.1

* Wed Jul 20 2022 fushanqing <fushanqing@kylinos.cn> - 1.4.5-1
- update to 1.4.5

* Mon May 23 2022 fushanqing <fushanqing@kylinos.cn> - 1.4.3-2
- fix CVE-2022-27650.

* Tue Mar 1 2022 fu-shanqing <fushanqing@kylinos.cn> - 1.4.3-1
- Update to 1.4.3

* Tue Aug 3 2021 fu-shanqing <fushanqing@kylinos.cn> - 0.20.1-1
- Package init
